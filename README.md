# My Neovim Config

Based on: [🚀Launch.nvim](https://github.com/LunarVim/Launch.nvim)

## Install

```
sudo apt install neovim

brew install neovim
```

## Install ripgrep

```
sudo apt install ripgrep
```

## Run Setup

```
nvim
```

